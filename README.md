FinalChecker
============

This project demonstrates a simple ExtendJ extension that reports which
variables and parameters in a Java program are effectively final.

The project is built using [Gradle][1] and the [JastAdd Gradle plugin][2]. A
short description of the build script is included in this README. Info about
cloning the project with Git is in the next section.

Cloning the Project
-------------------

Use these commands to clone the project and build it the first time:

    git clone --recursive git@bitbucket.org:extendj/final-checker.git
    cd finalchecker
    gradle jar

Make sure to include the `--recursive` in the clone command to get the ExtendJ
submodule.

If you forgot the `--recursive` flag, don't worry, just go into the newly cloned
project and run these commands:

    git submodule init
    git submodule update --depth 1

That should download the ExtendJ git repository into the local directory `extendj`.

Build and Run
-------------

If you have [Gradle][1] installed you can issue the following commands to
build and test the finalchecker tool:

    gradle jar
    java -jar finalchecker.jar testfiles/Test.java


If you don't have Gradle installed you can use the `gradlew.bat` (on Windows)
or `gradlew` (Unix) script instead. For example to build on Windows run the
following in a command prompt:

    gradlew jar

The `gradlew` scripts are wrapper scripts that will download a specific version
of Gradle locally and run it.

Using Eclipse
-------------

If you wish to use eclipse to explore this project you can tell gradle to
generate eclipse project files:

    gradle eclipse

Now you should be able to open eclipse and import the generated project.

Customizing the Project
-----------------------

If you use FinalChecker as a starting point for a ExtendJ extension you will
have to customize some things such as project name and main class name.
Typically you will have to edit the following files:

* `settings.gradle` - replace `'finalchecker'` by your project name
* `build.gradle` - edit the "mainClassName = " line with your main class name,
  and edit the "module = " line with the name of your extension
* `src/main/java/finalchecker/FinalChecker.java` this main class should be
  replaced by your own main class
* `modules` - this is the module definition file, we suggest that you change
  the first line which contains the name of the module/extension

Explanation of Files
--------------------

Here is a short explanation of what each file in the project base directory contains:

* `build.gradle` - the main Gradle build script. There is more info about this below!
* `settings.gradle` - only contains the project name, if this did not exist the
  project name would be the name of the directory containing `build.gradle`.
The project name affects the generated Jar filename.
*  `modules` - this file contains module definitions for the JastAdd build tool. This
  defines things such as which ExtendJ modules to include in the build, and where
additional JastAdd source files are located.
* `testfiles` - a directory for test files to run the final checker on.
* `finalchecker.jar` - generated finalchecker Jar file.
* `README.md` - this file
* `gradlew.bat` - windows Gradle wrapper script (explained above)
* `gradlew` - Unix Gradle wrapper script

Gradle Build Walkthrough
------------------------

The build script `build.gradle` may need an introduction even if you are already familiar with
[Gradle][1]. The build uses a custom [JastAdd Gradle plugin][2]. The plugin is available from the
Maven repository `http://jastadd.org/mvn/`, so the first part of the build script adds this
maven repository and a dependency for the JastAdd plugin:

    buildscript {
        repositories.mavenLocal()
        repositories.maven {
            url 'http://jastadd.org/mvn/'
        }
        dependencies {
            classpath group: 'org.jastadd', name: 'jastaddgradle', version: '1.9.4'
        }
    }

The next part is a list of Gradle plugins that we will use:

    apply plugin: 'java'
    apply plugin: 'application'
    apply plugin: 'jastadd'
    apply plugin: 'eclipse'

Next comes the `jastadd` configuration. This part provides the JastAdd plugin
specific settings needed to build the project:


    jastadd {
        modules 'modules'

        module = 'finalchecker' // TODO replace with your own module name

        astPackage = 'AST'
        genDir = 'src/gen/java'
        parser.name = 'JavaParser'
    }

The `modules 'modules'` line tells the JastAdd plugin where to find module
specification files. The list only contains one item: the local `modules`
file.

Each module specification file can define several modules, however our
finalchecker `modules` file defines only one module named `finalchecker`.  The
`include` construct is used to include the ExtendJ modules that the
finalchecker module depends on.

The `module = 'finalchecker'` line specifies the target module, i.e., the module
that the JastAdd plugin should build. If the target module is not found among
the list of module specifications then an error is reported during the build.

The next part of the build script has some simpler configuration things, such
as main class name, source and target Java versions, and the destination
directory for the Jar file:

    mainClassName = 'finalchecker.FinalChecker' // TODO replace with your own main class name
    jar.manifest.attributes 'Main-Class': mainClassName
    jar.destinationDir = projectDir

    // java -source and -target
    sourceCompatibility = '1.6'
    targetCompatibility = '1.6'

At the end of the build script there is some code needed to generate the
eclipse project files correctly when you run the `eclipse` build task. This
part is a bit technical but not necessary for understanding how the
finalchecker project is built. You can safely remove this part if you do not
need to generate eclipse projects using Gradle.

Additional Resources
--------------------

More examples about how to use the [JastAdd Gradle plugin][2] to build ExtendJ-like projects can be found here:

* [JastAdd Example: GradleBuild](http://jastadd.org/web/examples.php?example=GradleBuild)

[1]:https://gradle.org/
[2]:https://bitbucket.org/joqvist/jastaddgradle/overview
