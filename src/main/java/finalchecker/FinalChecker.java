/* Copyright (c) 2015, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package finalchecker;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Iterator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;

import org.extendj.ast.Program;
import org.extendj.ast.Problem;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.JavaParser;
import org.extendj.ast.BytecodeReader;
import org.extendj.ast.BytecodeParser;
import org.extendj.ast.AbstractClassfileParser;

/** Check for variables that can safely be declared 'final' in Java source code. */
public class FinalChecker {

  private final Program program = new Program();

  public FinalChecker() {
    program.initBytecodeReader(Program.defaultBytecodeReader());
    program.initJavaParser(Program.defaultJavaParser());
    
  }

  /** Run the analysis on the files listed as command-line arguments. */
  public static void main(String args[]) {
    int exitCode = new FinalChecker().run(args);
    if (exitCode != 0) {
      System.exit(exitCode);
    }
  }

  private int run(String args[]) {
    Collection<CompilationUnit> work = new LinkedList<CompilationUnit>();

    try {
      boolean compileError = false;

      if (args.length == 0) {
        System.err.println("Warning: no input files specified.");
      }

      for (String file : args) {
        if (!file.endsWith(".java") || !new File(file).isFile()) {
          System.err.println("Error: not a Java source file: " + file);
          compileError = true;
        } else {
          program.addSourceFile(file);
        }
      }

      // Process source compilation units.
      Iterator<CompilationUnit> iter = program.compilationUnitIterator();
      while (iter.hasNext()) {
        CompilationUnit unit = iter.next();
        if (unit.fromSource()) {
          try {
            Collection<Problem> errors = unit.parseErrors();
            Collection<Problem> warnings = Collections.emptyList();
            if (errors.isEmpty()) {
              errors = unit.problems();
            }
            if (!errors.isEmpty()) {
              System.err.println("Errors:");
              for (Problem problem : errors) {
                System.err.println(problem);
              }
              compileError = true;
            } else {
              if (!warnings.isEmpty()) {
                System.err.println("Warnings:");
                for (Problem problem : warnings) {
                  System.err.println(problem);
                }
              }
              processNoErrors(unit);
            }
          } catch (Error e) {
            System.err.println("Encountered error while processing " + unit.pathName());
            throw e;
          }
        }
      }

      if (compileError) {
        return 1;
      }
    } catch (AbstractClassfileParser.ClassfileFormatError e) {
      System.err.println(e.getMessage());
      return 1;
    } catch (Throwable t) {
      System.err.println("Fatal exception:");
      t.printStackTrace(System.err);
      return 1;
    } finally {
      if (program.options().hasOption("-profile")) {
        program.printStatistics(System.out);
      }
    }
    return 0;
  }

  private void processNoErrors(CompilationUnit unit) {
    if (unit != null && unit.fromSource()) {
      unit.checkFinalVariables();
    }
  }

}
